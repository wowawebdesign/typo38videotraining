lib.languagemenu = HMENU
lib.languagemenu {

	special = language
	special.value = 0,1,2

	wrap = <div class="languagemenu">|</div>

	1 = TMENU
	1 {
		NO = 1
		NO.stdWrap.override = DE || EN || FR
		NO {
			doNotLinkIt = 1
			stdWrap.typolink.parameter.data = page:uid
			stdWrap.typolink.additionalParams = &L=0 || &L=1 || &L=2
			stdWrap.typolink.addQueryString = 1
			stdWrap.typolink.addQueryString.exclude = L,id,cHash,no_cache
			stdWrap.typolink.addQueryString.method = GET
			stdWrap.typolink.useCacheHash = 1
		}

		ACT < .NO
		ACT {
			allWrap = <span class="curlang">|</span>
			doNotLinkIt = 1
			stdWrap.typolink >
		}

		USERDEF1 < .NO
		USERDEF1 {
			#allWrap = <span class="notranslation">|</span>
			#doNotLinkIt = 1
			doNotShowLink = 1
		}
	}

}