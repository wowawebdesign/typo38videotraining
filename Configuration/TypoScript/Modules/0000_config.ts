config {

	sys_language_uid = 0
	language = de
	locale_all = de_DE.utf-8
	htmlTag_langKey = de
	sys_language_overlay = hideNonTranslated

	linkVars = L

	spamProtectEmailAddresses = ascii
	#spamProtectEmailAddresses_atSubst = (==at==)
	#spamProtectEmailAddresses_lastDotSubst = (.)

	#concatenateCss = 1
	#compressCss = 1

	#concatenateJs = 1
	#compressJs = 1

	## title Tag verbessern
	pageTitleFirst = 1
	pageTitleSeparator = -
	pageTitleSeparator.noTrimWrap = | | |
}

## Englisch
[globalVar = GP:L=1]

config {
	sys_language_uid = 1
	language = en
	locale_all = en_GB.utf-8
	htmlTag_langKey = en
}

[global]

## Franzoesisch
[globalVar = GP:L=2]


	config {
		sys_language_uid = 2
		language = fr
		locale_all = fr_FR.utf-8
		htmlTag_langKey = fr
	}

[global]