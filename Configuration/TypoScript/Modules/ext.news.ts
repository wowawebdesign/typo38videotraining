plugin.tx_news {

	view.templateRootPaths.100 = EXT:typo38videotraining/Resources/Private/News/Templates/

	settings {
		displayDummyIfNoMedia = 1
		list.media.dummyImage = fileadmin/user_upload/bilder/lake-430508_1920.jpg

		detail.errorHandling = redirectToPage,57
	}
}

## Linkhanlder konfigurieren
## Quelle: https://usetypo3.com/linkhandler.html
config.recordLinks.tx_news {
	typolink {
		parameter = 58
		additionalParams.data = field:uid
		additionalParams.wrap = &tx_news_pi1[news]=|
		useCacheHash = 1
	}
}
