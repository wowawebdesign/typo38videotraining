lib.rootline = COA
lib.rootline {

	wrap = <div class="container rootline"><ol class="breadcrumb">|</ol></div>

	10 = HMENU
	10 {
		special = rootline
		special.range = 0|-1

		1 = TMENU
		1 {
			NO = 1
			NO {
				allWrap = <li class="breadcrumb-item">|</li>
				stdWrap.htmlSpecialChars = 1
				ATagTitle.field = description // subtitle // title
			}

			CUR < .NO
			CUR {
				doNotLinkIt = 1
				allWrap = <li class="breadcrumb-item active">|</li>
			}
		}
	}
}

[globalVar = GP:tx_news_pi1|news > 0]

lib.rootline.20 = RECORDS
lib.rootline.20 {
	tables = tx_news_domain_model_news
	dontCheckPid = 1
	source.data = GP:tx_news_pi1|news
	source.intval = 1
	conf.tx_news_domain_model_news = TEXT
	conf.tx_news_domain_model_news.field = title
	conf.tx_news_domain_model_news.htmlSpecialChars = 1
	wrap = <li class="breadcrumb-item active">|</li>
}

[global]